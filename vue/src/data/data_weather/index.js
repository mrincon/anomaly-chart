import getData from "./temperature_data.js";

function average(data, key) {
  var sum = data.reduce(function(sum, d) {
    return sum + (key ? d[key] : d);
  }, 0);

  var avg = sum / data.length;
  return avg;
}

export default function(sampleSize, windowSize, stdDevFactor){
    let data = getData()
    .reverse()
    .slice(-sampleSize);

  // process data
  data.forEach((d, index) => {
    // sort prev vals by val
    let rollingWindow = data
      .slice(index - windowSize, index)
      .sort((a, b) => a.val - b.val);

    if (rollingWindow.length == 0) {
      // noop
      d.lower = 0;
      d.line = 0;
      d.upper = 0;
      return;
    }

    // Use std. dev.
    const windowMean = average(rollingWindow, "val");

    const squareDiffs = rollingWindow.map(function(wd) {
      var diff = wd.val - windowMean;
      var sqr = diff * diff;
      return sqr;
    });

    const stdDev = Math.sqrt(average(squareDiffs));

    d.mean = windowMean;
    d.upper = windowMean + stdDev * stdDevFactor;
    d.lower = windowMean - stdDev * stdDevFactor;
  });

  const base = -data.reduce(function(min, d) {
    return Math.floor(Math.min(min, d.lower));
  }, Infinity);

  // start from after the initial rollingWindow
  const shownData = data.slice(windowSize);

  const main = shownData.map(d => {
    return [d.label, d.val];
  });

  const upper = shownData.map(d => {
    return [d.label, d.upper];
  });

  const mean = shownData.map(d => {
    return [d.label, d.mean];
  });

  const lower = shownData.map(d => {
    return [d.label, d.lower];
  });

  return {main, upper, mean, lower, base};

}
