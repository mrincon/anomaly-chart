import lower from "./lower";
import main from "./main";
import mean from "./mean";
import upper from "./upper";

const meanData = main.data.result[0].values.map(function(item) {
  const [xAxis] = item;
  const [, yAxis] = mean.data.result[0].values.find(meanItem => {
    return xAxis == meanItem[0];
  });
  return [xAxis, yAxis];
});

export default function() {
  return {
    base: 0,
    lower: lower.data.result[0].values,
    main: main.data.result[0].values,
    upper: upper.data.result[0].values,
    mean: meanData
  };
}
