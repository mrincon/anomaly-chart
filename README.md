# Anomaly Chart POC

This simple anomaly chart displays 

Data source: [https://datahub.io/core/global-temp](https://datahub.io/core/global-temp)

## Goal

Understand how to best display anomalies in a dataset of varying sizes.

## Project setup

```sh
cd vue/
yarn install
```

### Compiles and hot-reloads for development

```sh
cd vue/
yarn serve
```

### Compiles and minifies for production

```sh
cd vue/
yarn build # gets built in ../public
```
